# Protein fossils

These scripts were used to find the protein fossils described in:
[Paleozoic protein fossils illuminate the evolution of vertebrate
genomes and transposable elements][paper].  The hope is that you can
easily adapt them for other protein-fossil studies.

Note: `maf-linked` was moved from here into [LAST][].

## Human proteins

The paper used human non-LINE proteins from [UniProt][], with
"existence level" 1 to 3.  They were extracted like this:

    sprot-seqs uniprot_sprot_human.dat > human-prots.fa

## Getting protein fossils

As described in the paper, each genome was compared to protein
sequences, producing an alignment file `aln.maf`.  This was converted
to [PSL][] format:

    maf-convert psl aln.maf > aln.psl

The following scripts require that [seg-suite] version >= 95 is
installed (in your PATH).  We need genomic coordinates of known
protein-coding (CDS) regions, to discard alignments that overlap them.
For human, CDS regions were taken from these two `.txt` files
downloaded from the [UCSC Genome Browser][]:

    cds-from-gpd ncbiRefSeq.txt wgEncodeGencodeCompV37.txt > myCDS

For each non-human genome, CDS regions were taken from its `.gff` file
downloaded from [NCBI Genome][]:

    cds-from-gff genes.gff > myCDS

But if the genome comes from UCSC, we need to convert NCBI &rarr; UCSC
chromosome names, using each genome's `chromAlias.txt` file from UCSC:

    cds-from-gff genes.gff chromAlias.txt > myCDS

Now, we can get protein fossils derived from transposable elements (TEs):

    protein-fossils-te aln.psl myCDS > te.psl

And protein fossils derived from host genes:

    protein-fossils-hg aln.psl myCDS rmskFile > hg.psl

This uses a [RepeatMasker][] file for the genome (to discard
overlaps), which can be either a RepeatMasker `.out` file, or a UCSC
`rmsk` file such as `rmskOutCurrent.txt`.

## Conservation

The following scripts get protein fossils that are conserved between
genomes.  They use pair-wise genome alignments like
[these](https://github.com/mcfrith/last-genome-alignments).  As
mentioned in the paper, isolated inter-genome alignments were
discarded, by only keeping groups of nearby alignments:

    maf-linked genomeAlignment.maf > linked.maf

The next step gets aligned genome segments that don't overlap CDS
regions:

    maf-noncoding linked.maf myCDS1 myCDS2 > ncSegs

**Careful:** `myCDS1` should have CDS regions for the 1st/top genome
in the maf alignments, and `myCDS2` should have CDS regions for the
2nd/bottom genome.  Now we can get `psl` records that substantially
overlap inter-genome alignments:

    conserved-psl in.psl ncSegs genomeNum > cons.psl

`genomeNum` should be either `1` or `2`, indicating whether the `psl`
data is for the 1st/top genome or the 2nd/bottom genome.

## BED format

This converts PSL to [BED][] format:

    psl-to-bed my.psl > my.bed

Optionally, you can specify an [R,G,B color][]:

    psl-to-bed my.psl 255,0,0 > red.bed

Optionally, you can map the data from one genome to another, via
inter-genome alignments:

    psl-to-bed my.psl 255,0,0 linked.maf genomeNum > mapped.bed

`genomeNum` should be either `1` or `2`, indicating whether the input
`psl` data is for the 1st/top genome or the 2nd/bottom genome.  After
mapping to a different genome, segments that substantially overlap CDS
regions in that genome were discarded:

    bed-omit mapped.bed myCDS > my.bed

Finally, `add-bed` can be used to merge fossils found directly in a
genome with those mapped from other genomes.  This gets segments from
`in.bed` that don't overlap same-strand segments in `out.bed`, and
adds them to `out.bed`:

    add-bed in.bed out.bed

## Frameshifts and premature stop codons

You can count frameshifts and premature stop codons in DNA-to-protein
alignments, like this:

    maf-pseudogenes aln.maf > aln2.maf

DNA insertions >= 50 bases (adjustable with option `-i`) are assumed
to be introns, so don't contribute to frameshifts or stop codons.

[BED]: https://genome.ucsc.edu/FAQ/FAQformat.html#format1
[LAST]: https://gitlab.com/mcfrith/last
[NCBI Genome]: https://www.ncbi.nlm.nih.gov/genome
[PSL]: https://genome.ucsc.edu/FAQ/FAQformat.html#format2
[RepeatMasker]: http://www.repeatmasker.org/
[R,G,B color]: https://genome.ucsc.edu/FAQ/FAQformat.html#format1
[UCSC Genome Browser]: http://genome.ucsc.edu/
[UniProt]: https://www.uniprot.org/
[paper]: https://www.biorxiv.org/content/10.1101/2021.11.26.470093v1
[seg-suite]: https://github.com/mcfrith/seg-suite
